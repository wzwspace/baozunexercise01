package com.wzw.programming_conference.exception;

/**
 * @author wzw
 * @date 2023/3/28
 */
public class ConferenceException extends RuntimeException{

    public ConferenceException(String message) {
        super(message);
    }
}
