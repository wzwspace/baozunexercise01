package com.wzw.programming_conference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgrammingConferenceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgrammingConferenceApplication.class, args);
    }

}
