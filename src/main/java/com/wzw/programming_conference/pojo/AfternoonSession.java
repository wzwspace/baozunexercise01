package com.wzw.programming_conference.pojo;
import com.wzw.programming_conference.service.Imp.AbstractSession;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wzw
 * @date 2023/3/23
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class AfternoonSession extends AbstractSession {



    public AfternoonSession( int startTime, int fillTime, int endTime,int networkFillTime) {
        super(SessionName.AFTERNOON_SESSION, startTime, fillTime, endTime,networkFillTime);
    }

    public AfternoonSession( int startTime, int fillTime, int endTime) {
        super(SessionName.AFTERNOON_SESSION, startTime, fillTime, endTime);
    }

    public static  AfternoonSession getAfternoonSession(){
       return new AfternoonSession(TimePoint.AFTERNOON_START_TIME, TimePoint.AFTERNOON_START_TIME, TimePoint.NETWORKING_START_TIME,TimePoint.NETWORKING_START_TIME);
    }

}
