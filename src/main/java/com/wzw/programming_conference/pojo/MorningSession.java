package com.wzw.programming_conference.pojo;

import com.wzw.programming_conference.service.Imp.AbstractSession;
import lombok.*;

/**
 * @author wzw
 * @date 2023/3/23
 */
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
public class MorningSession extends AbstractSession {

    public MorningSession(int startTime, int fillTime, int endTime) {
        super(SessionName.MORNING_SESSION, startTime, fillTime, endTime);
    }
}
