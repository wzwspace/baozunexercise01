package com.wzw.programming_conference.pojo;

/**
 * @author wzw
 * @date 2023/3/22
 */
public class TimePoint {
    public static final int MORNING_START_TIME = 9 * 60; // 9AM in minutes
    public static final int LUNCH_START_TIME = 12 * 60; // 12PM in minutes
    public static final int AFTERNOON_START_TIME = 13 * 60; // 1PM in minutes
    public static final int NETWORKING_START_TIME = 16 * 60; // 4PM in minutes
    public static final int NETWORKING_END_START_TIME = 17 * 60; // 5PM in minutes

}
