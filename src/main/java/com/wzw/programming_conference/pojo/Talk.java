package com.wzw.programming_conference.pojo;

import lombok.*;

/**
 * @author wzw
 * @date 2023/3/21
 */
@Setter
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Talk implements Comparable<Talk> {

    private String schedule;
    private String title;
    private int duration; // in minutes

    public Talk(String title, int duration) {
        this.title = title;
        this.duration = duration;
    }


    @Override
    public int compareTo(Talk other) {
        return Integer.compare(duration, other.duration);
    }


}
