package com.wzw.programming_conference.pojo;

import com.wzw.programming_conference.service.Imp.AbstractSession;
import com.wzw.programming_conference.service.Session;
import lombok.*;

import java.util.*;

/**
 * @author wzw
 * @date 2023/3/21
 */

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Track {
    private int number;
    private List<Session> sessions;


    public Track(int number) {
        this.number = number;
        this.sessions = new ArrayList<>();
    }


    public Session getAfterNoonSession(){
        Optional<Session> session = sessions.stream().filter(ses-> ses.getName().equals(SessionName.AFTERNOON_SESSION)).findAny();
        return session.get();
    }

    public Session getMorningSession(){
        Optional<Session> session = sessions.stream().filter(ses-> ses.getName().equals(SessionName.MORNING_SESSION)).findAny();
        return session.get();
    }

    public Session addSession(Session session) {
        sessions.add(session);
        return session;
    }


    public static Track getTrack(int number) {
        return new Track(number);
        }

}
