package com.wzw.programming_conference.pojo;

/**
 * @author wzw
 * @date 2023/3/22
 */

public enum SessionName {
    MORNING_SESSION,AFTERNOON_SESSION;
}
