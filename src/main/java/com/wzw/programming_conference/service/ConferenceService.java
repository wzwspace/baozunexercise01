package com.wzw.programming_conference.service;

import com.wzw.programming_conference.pojo.Talk;
import com.wzw.programming_conference.pojo.Track;

import java.util.List;
import java.util.Map;

/**
 * @author wzw
 * @date 2023/3/21
 */
public interface ConferenceService {

    List<Talk> listStringToTalk(List<String> input);

    List<Talk> sortTalkByDuration(List<Talk> allTalks);

    Map<String, Track> fillTalkToTrack(List<Talk> talks);

    List<String> readMeetingFile(String fileName);

}
