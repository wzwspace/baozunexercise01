package com.wzw.programming_conference.service;

import com.wzw.programming_conference.pojo.SessionName;
import com.wzw.programming_conference.pojo.Talk;

import java.util.List;

/**
 * @author wzw
 * @date 2023/3/23
 */
public interface Session {

    boolean canAddTalk(Talk talk);

    boolean addTalk(Talk talk);

    boolean addNetWorkEvent(int duration);

    SessionName getName();

    List<Talk> getTalks();

}
