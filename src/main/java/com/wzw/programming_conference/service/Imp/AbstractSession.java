package com.wzw.programming_conference.service.Imp;
import com.wzw.programming_conference.pojo.SessionName;
import com.wzw.programming_conference.pojo.Talk;
import com.wzw.programming_conference.pojo.TimePoint;
import com.wzw.programming_conference.service.Session;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.ArrayList;
import java.util.List;

import static com.wzw.programming_conference.pojo.TimePoint.*;

/**
 * @author wzw
 * @date 2023/3/23
 */
@Data
@NoArgsConstructor
public class AbstractSession implements Session {
    private SessionName name;
    private int startTime;
    private int fillTime;
    private int networkFillTime;
    private int endTime;
    private List<Talk> talks=new ArrayList<>();


    public AbstractSession(SessionName name, int startTime, int fillTime, int endTime) {
        this.name = name;
        this.startTime = startTime;
        this.fillTime = fillTime;
        this.endTime = endTime;
    }

    public AbstractSession(SessionName name, int startTime, int fillTime, int endTime,int networkFillTime) {
        this.name = name;
        this.startTime = startTime;
        this.fillTime = fillTime;
        this.endTime = endTime;
        this.networkFillTime=networkFillTime;
    }

    @Override
    public boolean canAddTalk(Talk talk) {
        int newFillTime = fillTime + talk.getDuration();
        if (name.equals(SessionName.MORNING_SESSION)){
            return newFillTime  <=LUNCH_START_TIME&& !talks.contains(talk);
        }
        return newFillTime  <= NETWORKING_END_START_TIME && !talks.contains(talk);
    }
    @Override
    public boolean addTalk(Talk talk) {

        talks.add(talk);
        fillTime += talk.getDuration();
        talk.setSchedule(formatTime(fillTime-talk.getDuration()));
        return true;
    }
    @Override
    public boolean addNetWorkEvent(int duration) {
        if (name.equals(SessionName.AFTERNOON_SESSION)&&
                fillTime<= TimePoint.NETWORKING_END_START_TIME){
            return addTalk(new Talk("NetWork Event",duration));
        }
        return false;
    }

    protected String formatTime(int timeInMinutes) {
        int hour = timeInMinutes / 60;
        int minute = timeInMinutes % 60;
        String amPm = hour < 12 ? "AM" : "PM";
        if (hour > 12) {
            hour -= 12;
        }
        return String.format("%d:%02d%s", hour, minute, amPm);
    }
}
