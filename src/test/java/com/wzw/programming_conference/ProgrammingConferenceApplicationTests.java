package com.wzw.programming_conference;
import com.google.common.base.CharMatcher;
import com.wzw.programming_conference.pojo.*;
import com.wzw.programming_conference.service.ConferenceService;
import com.wzw.programming_conference.service.Imp.ConferenceServiceImp;
import com.wzw.programming_conference.service.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

@SpringBootTest
class ProgrammingConferenceApplicationTests {

    private static final Logger logger = LogManager.getLogger(ProgrammingConferenceApplicationTests.class);
    @Autowired
    ConferenceService conferenceService;


    @org.junit.jupiter.api.Test
    void testConferenceProgram() {
        List<Talk> talks = conferenceService.listStringToTalk(conferenceService.readMeetingFile("meeting.txt"));
        conferenceService.sortTalkByDuration(talks);//按时间排序
        Map<String, Track> trackMap = conferenceService.fillTalkToTrack(talks);
        Assertions.assertFalse(trackMap.isEmpty());
        for (String key : trackMap.keySet()) {
            logger.info("Track{}==========",key);
            Track track = trackMap.get(key);
            List<Session> sessions = track.getSessions();
            for (Session session : sessions) {
                if (session.getName().equals(SessionName.AFTERNOON_SESSION)) {
                    logger.info("12:00AM Lunch");
                }
                session.addNetWorkEvent(30);
                logger.info(session.getName().toString().toLowerCase());
                List<Talk> talks1 = session.getTalks();
                for (Talk talk : talks1) {
                    logger.info(talk.getSchedule() + "  " + talk.getTitle() + "==>" + talk.getDuration());
                }
            }
        }
    }

    @Test
    void testTalkTitleHaveDigit(){
//     Assertions.assertTrue(ConferenceServiceImp.titleDigitJudge("abc123 "));
     Assertions.assertFalse(ConferenceServiceImp.titleHaveDigit("a！@#￥%……&*（）!@#$%^&*(bc"));
//     Assertions.assertFalse(ConferenceServiceImp.titleDigitJudge("efg "));
    }
    @Test
     void testFileRead(){
        List<String> talkList = conferenceService.readMeetingFile("meeting.txt");
//        List<String> talkList = conferenceService.readMeetingFile(null);
//        List<String> talkList = conferenceService.readMeetingFile("aaa.txt");
        talkList.forEach(System.out::println);
        Assertions.assertFalse(talkList.isEmpty());
    }
    @Test
    void testListStringToTalk(){
//        List<Talk> talks = conferenceService.listStringToTalk(conferenceService.readMeetingFile("meeting.txt"));
        List<Talk> talks = conferenceService.listStringToTalk(conferenceService.readMeetingFile("meeting.txt"));
        talks.forEach(System.out::println);
        Assertions.assertFalse(talks.isEmpty());
    }
    @Test
    void testFillTalkToTrack(){
        List<Talk> talks = conferenceService.listStringToTalk(conferenceService.readMeetingFile("meeting.txt"));
        Map<String, Track> trackMap = conferenceService.fillTalkToTrack(talks);
        trackMap.keySet().forEach(key->System.out.println(key+"====>"+trackMap.get(key)));
        Assertions.assertFalse(trackMap.isEmpty());
    }
    @Test
    void testSortTalkByDuration(){
        List<Talk> talks = conferenceService.listStringToTalk(conferenceService.readMeetingFile("meeting.txt"));
       talks.forEach(System.out::println);
       logger.info("----------------sort-------------------");
        conferenceService.sortTalkByDuration(talks);
        Assertions.assertFalse(talks.isEmpty());
        talks.forEach(System.out::println);
    }
    @Test
    void testSessionMethod(){
        MorningSession morningSession = new MorningSession(TimePoint.MORNING_START_TIME,TimePoint.MORNING_START_TIME,TimePoint.LUNCH_START_TIME);
        AfternoonSession afternoonSession = new AfternoonSession(TimePoint.AFTERNOON_START_TIME, TimePoint.AFTERNOON_START_TIME, TimePoint.NETWORKING_END_START_TIME, TimePoint.NETWORKING_START_TIME);
        Talk talk = new Talk("12:00AM", "title", 20);
       Assertions.assertTrue(morningSession.canAddTalk(talk));
        Assertions.assertTrue(morningSession.addTalk(talk));
        Assertions.assertFalse(morningSession.addNetWorkEvent(60));
        Assertions.assertTrue(afternoonSession.canAddTalk(talk));
        Assertions.assertTrue(afternoonSession.addTalk(talk));
        Assertions.assertTrue(afternoonSession.addNetWorkEvent(60));
    }


    @Test
    void testHandlerLightning(){
        System.out.println(ConferenceServiceImp.handlerLighting("10lightning"));

    }



















    @Test
    void testLocalDateTime() {
        Duration duration = Duration.between(LocalDateTime.now(), LocalDateTime.of(2023, 3, 21, 22, 0));
        Assertions.assertNotNull(duration);
        logger.info(duration.toMinutes() + "--------------------------");
    }

    //先测试字符串拆分
    @Test
    void test3() {
        List<String> list = Arrays.asList("Writing Fast Tests Against Enterprise Rails 60min",
                "Overdoing it in Python 45min",
                "Lua for the Masses 30min",
                "Ruby Errors from Mismatched Gem Versions 45min",
                "Common Ruby Errors 45min",
                "Rails for Python Developers lightning",
                "Communicating Over Distance 60min",
                "Accounting-Driven Development 45mi",
                "Woah 30mi",
                "Sit Down and Write 30min",
                "Pair Programming vs Noise 45min",
                "Rails Magic 60min",
                "Ruby on Rails: Why We Should Move On 60min",
                "Clojure Ate Scala (on my project) 45min",
                "Programming in the Boondocks of Seattle 30min",
                "Ruby vs. Clojure for Back-End Development 30min",
                "Ruby on Rails Legacy App Maintenance 60min",
                "A World Without HackerNews 30min",
                "User Interface CSS in Rails Apps 30min");
        ArrayList<Talk> arrayList = new ArrayList();
        for (String s : list) {
            Integer time = Integer.parseInt(CharMatcher.JAVA_DIGIT.retainFrom(s));
            String title = s.replaceAll("[0-9]+[a-z]+", "");
            logger.info(title + "=======" + time);
            Talk talk = new Talk(title, time);
            arrayList.add(talk);
        }
    }


    @Test
     void testHandlerString(){
        ArrayList<String> list = new ArrayList<>(Arrays.asList("Writing Fast Tests Against Enterprise Rails 60min",
                "Overdoing it in Python 45min",
                "Lua for the Masses 30min",
                "Ruby Errors from Mismatched Gem Versions 45min",
                "Common Ruby Errors 45min",
                "Rails for Python Developers lightning",//lightning
                "Communicating Over Distance 60min",
                "Accounting-Driven Development 45min",
                "Woah 30min",
                "Sit Down and Write 30min",
                "Pair Programming vs Noise 45min",
                "Rails Magic 60min",
                "Ruby on Rails: Why We Should Move On 60min",
                "Clojure Ate Scala (on my project) 45min",
                "Programming in the Boondocks of Seattle 30min",
                "Ruby vs. Clojure for Back-End Development 30min",
                "Ruby on Rails Legacy App Maintenance 60min",
                "A World Without HackerNews 30min",
                "User Interface CSS in Rails Apps 30min"));

        list.stream()
                .map(str->str.endsWith("lightning")?str.replace("lightning","5min"):str)
                .map(str->new Talk(str.substring(0,str.lastIndexOf(" ")),
                        Integer.parseInt(str.substring(str.lastIndexOf(" ")).replace("min","")
                                .trim()))).forEach(System.out::println);

    }

    @Test
     void testGoogleTools(){
        String dijit = CharMatcher.javaDigit().retainFrom("abc");
        System.out.println(dijit);
        Assertions.assertNull(dijit);

    }

}








